import {StatusBar} from 'expo-status-bar';
import {ImageBackground, StyleSheet, Text, TouchableOpacity} from 'react-native';


export default function ScreenC({ navigation }){
    const background = require("../assets/Background.jpg");


  return (
      <ImageBackground
          style={styles.body}
          source={background}>
          <StatusBar style="auto"/>
          <TouchableOpacity
              accessible={true}
              accessibilityLabel={"KLICKA HÄR"}
              onPress={() => {
                  console.log("Hello")
              }}>
              <Text style={styles.header}>HELLO</Text>
          </TouchableOpacity>
      </ImageBackground>
  );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        fontSize: 100,
        marginTop: 10,
        marginBottom: 10,
        justifyContent: 'center',
        color: "#511f1f",
        fontWeight: "bold",
        textAlign: "center",
    },
});
