import * as React from 'react';
import {useState} from 'react';
import {StatusBar} from 'expo-status-bar';
import {Image, ImageBackground, Modal, Pressable, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import InputButton from "../components/InputButton";
import ScreenA from "./ScreenA";
import Button1 from "../components/Button1";
import Button2 from "../components/Button2";
import Button3 from "../components/Button3";
import ScreenB from "./ScreenB";
import ScreenC from "./ScreenC";


export default function Home({navigation}) {

    const back = require("../assets/back.png");
    const [name, setName] = useState('');
    const [submitted, SetSubmitted] = useState(false);
    const [showWarning, SetshowWarning] = useState(false);

    const onPressHandler1 = () => {
        navigation.navigate(ScreenA);
    }
    const onPressHandler2 = () => {
        navigation.navigate(ScreenB);
    }
    const onPressHandler3 = () => {
        navigation.navigate(ScreenC);
    }

    const onPressHandler4 = () => {
        SetshowWarning(true);
        SetSubmitted(!submitted);
    }


    return (
        <ImageBackground
            style={styles.body}
            source={back}>
            <StatusBar style="auto"/>
            <View style={styles.buttonContainer}>
                <Button1 title={'Go to Screen A'}
                         onPress={onPressHandler1}/>
                <Button2 title={'Go to Screen B'}
                         onPress={onPressHandler2}/>
                <Button3 title={'Go to Screen C'}
                         onPress={onPressHandler3}/>
            </View>
            <InputButton title={submitted ? 'Clear' : 'Submit'} value={name} onChangeText={setName}
                         onPress={onPressHandler4}/>
            {getModal(showWarning, SetshowWarning)}
            {
                submitted ?
                    <View style={styles.body1}>
                        <Text style={styles.text1}>
                            HEJ {name}!!!!!
                        </Text>
                    </View>
                    :
                    <View style={styles.body1}>
                        <Text style={styles.text1}>
                            HEJ!!!!!
                        </Text>
                    </View>
            }
            <View style={styles.buttonContainer}>
                <Image
                    style={styles.image1}
                    source={back}
                    resizeMode='stretch'
                />
                <Image
                    style={styles.image1}
                    source={back}
                    resizeMode='stretch'
                />
                <Image
                    style={styles.image1}
                    source={back}
                    resizeMode='stretch'
                />
            </View>
        </ImageBackground>
    );
}

function getModal(showWarning, SetshowWarning) {
    return <Modal
        visible={showWarning}
        transparent
        onRequestClose={() =>
            SetshowWarning(false)
        }
        animationType='slide'
        hardwareAccelerated
    >
        <View style={styles.centered_view}>
            <View style={styles.warning_modal}>
                <View style={styles.warning_title}>
                    <Text style={styles.text}>Hello</Text>
                </View>
                <View style={styles.warning_body}>
                    <Text style={styles.text2}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus purus in massa tempor. Faucibus et molestie ac feugiat sed. Facilisis volutpat est velit egestas dui id. Netus et malesuada fames ac turpis. Vitae auctor eu augue ut lectus arcu bibendum at varius. Vel pretium lectus quam id leo. Nunc aliquet bibendum enim facilisis gravida neque convallis a cras. Pellentesque dignissim enim sit amet. Vitae proin sagittis nisl rhoncus mattis rhoncus urna neque viverra. Urna et pharetra pharetra massa massa ultricies mi quis hendrerit. Et magnis dis parturient montes nascetur ridiculus mus mauris. Nam aliquam sem et tortor consequat id porta nibh venenatis. Mi in nulla posuere sollicitudin aliquam. Suscipit adipiscing bibendum est ultricies integer quis. Arcu vitae elementum curabitur vitae nunc sed. Aliquet bibendum enim facilisis gravida neque convallis a cras semper. Velit sed ullamcorper morbi tincidunt. Etiam dignissim diam quis enim lobortis. Nisl nisi scelerisque eu ultrices vitae auctor eu.</Text>
                </View>
                <Pressable
                    onPress={() => SetshowWarning(false)}
                    style={styles.warning_button}
                    android_ripple={{color: '#cf1a1a'}}
                >
                    <Text style={styles.text}>OK</Text>
                </Pressable>
            </View>
        </View>
    </Modal>;
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text1: {
        fontSize: 40,
        color: "#0d1754",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    buttonContainer: {
        height: 150,
        width: 400,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 12,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-evenly",
    },
    body1: {
        flex: 1,
        alignItems: 'center',
    },
    text: {
        color: '#60ee11',
        fontSize: 20,
        textAlign: 'center',
    },
    text2: {
        fontSize: 5,
        color: "#4e047a",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    centered_view: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f80bdd'
    },
    warning_modal: {
        width: 200,
        height:200,
        backgroundColor: '#49f8c6',
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 20,
    },
    warning_title: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffff00',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },
    warning_body: {
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    warning_button: {
        backgroundColor: '#f66262',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    image1: {
        width: 100,
        height: 170,
        margin: 10,
    }
});
