import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Pressable, Image,
} from 'react-native';
import ScreenC from "./ScreenC";

export default function ScreenB({ navigation }) {
    const hund = require("../assets/hund2.png");

    const onPressHandler = () => {
        navigation.navigate(ScreenC);
    }

    return (
        <View style={styles.body}>
            <Text style={styles.text}>
                Screen B
            </Text>
            <Pressable
                onPress={onPressHandler}
                style={({ pressed }) => ({ backgroundColor: pressed ? '#ddd' : '#00ff00' })}
            >
                <Text style={styles.text}>
                    Go to Screen C
                </Text>
            </Pressable>
            <Image
                style={styles.image1}
                source={hund}
                resizeMode='stretch'
            />
            <Text style={styles.text}>
                BILD PÅ EN KATT
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#179133'
    },
    text: {
        fontSize: 10,
        fontWeight: 'bold',
        color: '#3ba53d'
    },
    image1: {
        marginTop: 40,
        width: 100,
        height: 170,
        margin: 10,
    }
})
