import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Pressable, Image,
} from 'react-native';
import ScreenB from "./ScreenB";

export default function ScreenA({ navigation }) {
    const back = require("../assets/back.png");

    const onPressHandler = () => {
        navigation.navigate(ScreenB);
    }

    return (
        <View style={styles.body}>
            <Text style={styles.text}>
                Screen A
            </Text>
            <Image
                style={styles.image1}
                source={back}
                resizeMode='stretch'
            />
            <Pressable
                onPress={onPressHandler}
                style={({ pressed }) => ({ backgroundColor: pressed ? '#ddd' : '#0f0' })}
            >
                <Text style={styles.text}>
                    Go to Screen B
                </Text>
            </Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#c81b1b',
        justifyContent: 'space-between',
        margin: 20,
        alignItems: 'center',
    },
    text: {
        fontSize: 10,
        fontWeight: 'bold',
    },
    image1: {
        width: 100,
        height: 100,
        margin: 10,
    }
})
