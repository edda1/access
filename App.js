import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from "./screens/Home";
import ScreenC from "./screens/ScreenC";
import ScreenA from "./screens/ScreenA";
import ScreenB from "./screens/ScreenB"


const App = () => {
    const Stack = createNativeStackNavigator();

  return (
          <NavigationContainer>
              <Stack.Navigator initialRouteName="Home">
                  <Stack.Screen
                      name="Home"
                      component={Home}
                      options={{title: 'Welcome'}}
                  />
                  <Stack.Screen
                      name="ScreenA"
                      component={ScreenA}/>
                  <Stack.Screen
                      name="ScreenB"
                      component={ScreenB}/>
                  <Stack.Screen
                      name="ScreenC"
                      component={ScreenC}/>
              </Stack.Navigator>
          </NavigationContainer>
  );
}

export default App;
