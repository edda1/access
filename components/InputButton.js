import React from "react";
import {StyleSheet, View, Text, TextInput, TouchableOpacity} from "react-native";

TouchableOpacity.defaultProps = {activeOpacity: 0.8};

const InputButton = ({onPress, title, onChangeText, value}) => (
    <View style={styles.submitButtonContainer}>
        <View>
            <TextInput
                style={styles.input}
                placeholder="Enter your name"
                onChangeText={onChangeText}
                value={value}
            />
        </View>
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={onPress}
        >
            <Text style={styles.submitButtonText}>{title}</Text>
        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
    submitButtonContainer: {
        elevation: 8,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: "#6997a2",
        paddingHorizontal: 12,
        marginTop: 20,
        width: 300
    },
    submitButtonText: {
        fontSize: 18,
        color: "#a75a5a",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    input: {
        marginTop: 5,
        marginBottom: 20,
        width: 200,
        fontSize: 10,
        alignSelf: "center",
        borderWidth: 1,
        borderColor: "#28280d",
        borderStyle: "dashed",
    },
});

export default InputButton;
