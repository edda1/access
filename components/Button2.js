import React from "react";
import {StyleSheet, Text, Pressable} from "react-native";



const Button2 = ({onPress, title}) => (
        <Pressable
            onPress={onPress}
            style={({pressed}) => ({backgroundColor: pressed ? '#1726ac' : '#ff0000'})}
        >
            <Text style={styles.submitButtonText}>{title}</Text>
        </Pressable>
);

const styles = StyleSheet.create({

    submitButtonText: {
        fontSize: 20,
        color: "#806736",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "lowercase"
    },
});

export default Button2;
