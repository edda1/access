import React from "react";
import {StyleSheet, Text, Pressable} from "react-native";



const Button3 = ({onPress, title}) => (
        <Pressable
            onPress={onPress}
            style={({pressed}) => ({backgroundColor: pressed ? '#6d1a69' : '#000dff'})}
        >
            <Text style={styles.submitButtonText}>{title}</Text>
        </Pressable>
);

const styles = StyleSheet.create({

    submitButtonText: {
        fontSize: 16,
        color: "#2d7956",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
});

export default Button3;
