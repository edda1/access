import React from "react";
import {StyleSheet, Text, Pressable} from "react-native";


const Button1 = ({onPress, title}) => (
        <Pressable
            onPress={onPress}
            style={({pressed}) => ({backgroundColor: pressed ? '#ddd' : '#0f0'})}
        >
            <Text style={styles.submitButtonText}>{title}</Text>
        </Pressable>
);

const styles = StyleSheet.create({

    submitButtonText: {
        fontSize: 12,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
});

export default Button1;
